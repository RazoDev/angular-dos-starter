'use strict';

const argv = require('yargs').argv;

const ENVS = {
    DEV: 'development',
    PROD: 'production',
    TEST: 'testing'
};

const ENV = argv.env || process.env.NODE_ENV || ENVS.DEV;

module.exports = {
    ENV: ENV,
    ENVS: ENVS
};
