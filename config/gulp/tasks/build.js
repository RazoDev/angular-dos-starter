'use strict';
const gulp        = require('gulp');
const runSequence = require('run-sequence');
const config      = require('../config')();
const useref      = require('gulp-useref');
const gulpif      = require('gulp-if');
const rev         = require('gulp-rev');
const revReplace  = require('gulp-rev-replace');
const uglify      = require('gulp-uglify');
const cssnano     = require('gulp-cssnano');
const Builder     = require('systemjs-builder');
const strip       = require('gulp-strip-comments');
const imagemin    = require('gulp-imagemin');

gulp.task('build', (done) => {
    runSequence('build-systemjs', 'build-assets', done);
});

gulp.task('build-systemjs', (done) => {
    runSequence('tsc-app', buildSJS);

    function buildSJS () {
        const builder = new Builder();
        builder.loadConfig(config.src + 'systemjs.conf.js')
        .then(() => {
            const path = config.tmpApp;
            return builder
                .buildStatic(
                    path + 'main.js', //input file to bundle
                    path + 'bundle.js', // output file bundled
                    config.systemJs.builder); // bundle cfg
        })
        .then(() => {
            console.log('Build complete');
            done();
        })
        .catch((ex) => {
            console.log('error', ex);
            done('Build failed.');
        });
    }
});

/* Concat and minify/uglify all css, js, and copy fonts */
gulp.task('build-assets', (done) => {
    runSequence('clean-build', ['sass','fonts'], () => {

        gulp.src(config.app + '**/*.html', {
            base: config.app
        })
        .pipe(strip())
        // .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(config.build.app));

        gulp.src(config.cssStyles, {
            base: config.root
        })
        .pipe(cssnano())
        .pipe(gulp.dest(config.build.app));

        gulp.src(config.src + 'favicon.ico')
        .pipe(gulp.dest(config.build.path));

        gulp.src(config.assetsPath.images + '**/*.*', {
            base: config.assetsPath.images
        })
        .pipe(imagemin())
        .pipe(gulp.dest(config.build.assetPath + 'images'));

        gulp.src(config.phpFiles + '**/*.*' ,{
          base: config.src
        })
        .pipe(gulp.dest(config.build.path));

        gulp.src(config.index)
            .pipe(useref())
            .pipe(gulpif('assets/lib.js', uglify()))
            .pipe(gulpif('*.css', cssnano()))
            .pipe(gulpif('!*.html', rev()))
            .pipe(revReplace())
            .pipe(gulp.dest(config.build.path))
            .on('finish', done);
    });
});

/* Copy fonts in packages */
gulp.task('fonts', () => {
    gulp.src(config.assetsPath.fonts + '**/*.*', {
        base: config.assetsPath.fonts
    })
    .pipe(gulp.dest(config.build.fonts));

    gulp.src([
        'bower_components/components-font-awesome/fonts/*.*',
        'bower_components/mdi/fonts/*.*'
    ])
    .pipe(gulp.dest(config.build.fonts));
});
