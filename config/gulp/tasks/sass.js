'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const config = require('../config')();

const allFiles = [].concat(config.assetsPath.styles + '**/*.scss', config.app + '**/*.scss');

gulp.task('sass', () => {
    return sassCompiling();
});

gulp.task('watch-sass', () => {
    gulp.watch(allFiles, (file) => {
      console.log('Sass compiling ' + file.path + '...' );
      return sassCompiling();
    });
});

function sassCompiling() {
  return gulp.src(config.assetsPath.styles + 'main.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest(config.assetsPath.styles));
}
