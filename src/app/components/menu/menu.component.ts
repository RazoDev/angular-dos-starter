import {Component, Input, ChangeDetectionStrategy, AfterViewInit} from '@angular/core';
import {CORE_DIRECTIVES} from '@angular/common';
import {ROUTER_DIRECTIVES} from '@angular/router';

@Component({
    selector: 'rexdev-menu',
    templateUrl: 'app/components/menu/menu.html',
    directives: [ROUTER_DIRECTIVES, CORE_DIRECTIVES]
})
export class MenuComponent {

}
