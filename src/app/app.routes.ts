import { provideRouter, RouterConfig } from '@angular/router';
import {HomeComponent} from './components/home/home.component';

import{ServiciosComponent} from './components/servicios/servicios.component';

const routes: RouterConfig = [
  {path: '', component: HomeComponent},
  {path: 'servicios', component: ServiciosComponent},

];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];
