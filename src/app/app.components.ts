import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';
import {MenuComponent} from './components/menu/menu.component';

declare var $: any;
declare var foundation: any;

@Component({
    selector: 'rexdev-main-app',
    templateUrl: 'app/app.html',
    directives: [MenuComponent, ROUTER_DIRECTIVES]
})
export class AppComponent implements OnInit {

  ngOnInit(){
    $(document).foundation();
  }
  
}
